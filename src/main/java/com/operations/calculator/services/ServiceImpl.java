package com.operations.calculator.services;

import com.operations.calculator.thrift.TCalculatorService;
import com.operations.calculator.thrift.TDivisionByZeroException;
import com.operations.calculator.thrift.TOperation;

import org.apache.thrift.TException;

public class ServiceImpl implements TCalculatorService.Iface {

    @Override
    public int calculate(int num1, int num2, TOperation op) throws TDivisionByZeroException, TException {
        switch(op) {
            case ADD:
                return num1 + num2;
            case SUBTRACT:
                return num1 - num2;
            case MULTIPLY:
                return num1 * num2;
            case DIVIDE:
                try {
                    return num1 / num2;
                } catch(IllegalArgumentException e) {
                    throw new TDivisionByZeroException();
                }
            default:
                throw new TException("Operacion Desconocida " + op);
        }
    }
}