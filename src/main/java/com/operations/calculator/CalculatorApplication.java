package com.operations.calculator;

import com.operations.calculator.server.Server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CalculatorApplication {
	private static Server thriftServer;

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(CalculatorApplication.class, args);
        try {
            thriftServer = context.getBean(Server.class);
            thriftServer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
