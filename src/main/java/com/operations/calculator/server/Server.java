package com.operations.calculator.server;

import com.operations.calculator.services.ServiceImpl;
import com.operations.calculator.thrift.TCalculatorService.Iface;
import com.operations.calculator.thrift.TCalculatorService.Processor;

import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.springframework.stereotype.Component;
@Component
public class Server {

    public void start() {
        try {
            final Processor<Iface> processor = new Processor<Iface>(new ServiceImpl());

            Runnable simple = new Runnable() {
                public void run() {
                    simple(processor);
                }
            };

            new Thread(simple).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void simple(Processor<Iface> processor) {
        try {
            TServerTransport serverTransport = new TServerSocket(9090);
            TServer server = new TSimpleServer(new Args(serverTransport).processor(processor));

            System.out.println("Servidor Iniciado ...");
            server.serve();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}