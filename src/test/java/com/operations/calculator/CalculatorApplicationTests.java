package com.operations.calculator;

import com.operations.calculator.thrift.TOperation;
import com.operations.calculator.thrift.TCalculatorService.Client;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorApplicationTests {

	private static final String VALUES = "(%d %d): %d";

	@Test
	public void contextLoads() {
		
	}
	public static void main(String[] args) {
			try {
				TTransport transport = new TSocket("localhost", 9090);
				transport.open();

				TProtocol protocol = new TBinaryProtocol(transport);
				Client client = new Client(protocol);

				System.out.println(String.format("SUMA "+VALUES, 2, 3, client.calculate(2, 3, TOperation.ADD)));
				System.out.println(String.format("RESTA "+VALUES, 2, 3, client.calculate(2, 3, TOperation.SUBTRACT)));
				System.out.println(String.format("MULT "+VALUES, 2, 3, client.calculate(2, 3, TOperation.MULTIPLY)));
				System.out.println(String.format("DIV "+VALUES, 2, 3, client.calculate(2, 3, TOperation.DIVIDE)));
		
				transport.close();
				} catch (TException e) {
				e.printStackTrace();
				} 
    }
}
